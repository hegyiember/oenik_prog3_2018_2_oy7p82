﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using CarShop.Data;
    using CarShop.Logic;
    using CarShop.Tools;

    /// <summary>
    /// The main menu
    /// </summary>
    public class Program
    {
        private static ProjectTools.TableTypes enu;
        private static BusinessModellLogic bML;
        private static BusinessExtraLogic bEL;
        private static BusinessCompanyLogic bCL;
        private static BusinessNonCRUDLogic bNCL;
        private static ProjectTools.NonCRUDTypes enu2;

        /// <summary>
        /// Table type choosing menu
        /// </summary>
        public static void GetEnum()
        {
            enu = ProjectTools.TableTypes.PLACEHOLDER;
            while (true)
            {
                CursosPos.Clrscr();

                Console.ForegroundColor = ConsoleColor.Red;
                CursosPos.Gotoxy(10, 4);
                Console.Write("Welcome to Rent-A-Car 0.4!");

                Console.ForegroundColor = ConsoleColor.DarkCyan;
                CursosPos.Gotoxy(10, 5);
                Console.Write("-------------------------------------------");

                Console.ForegroundColor = ConsoleColor.Red;
                CursosPos.Gotoxy(10, 7);
                Console.Write("Choose the category (modell/extra/company): ");

                Console.ForegroundColor = ConsoleColor.DarkCyan;
                CursosPos.Gotoxy(10, 10);
                Console.Write("-------------------------------------------");

                Console.ForegroundColor = ConsoleColor.White;
                CursosPos.Gotoxy(10, 8);
                string name;
                name = Console.ReadLine();
                if (name.Equals(ProjectTools.TableTypes.MODELL.ToString().ToLower()))
                {
                    enu = ProjectTools.TableTypes.MODELL;
                }
                else if (name.Equals(ProjectTools.TableTypes.EXTRA.ToString().ToLower()))
                {
                    enu = ProjectTools.TableTypes.EXTRA;
                }
                else if (name.Equals(ProjectTools.TableTypes.COMPANY.ToString().ToLower()))
                {
                    enu = ProjectTools.TableTypes.COMPANY;
                }
                else
                {
                    CursosPos.Gotoxy(10, 12);
                    Console.WriteLine("Sorry, please write the input again.");
                    Console.ReadKey();
                    continue;
                }

                if (enu != ProjectTools.TableTypes.PLACEHOLDER)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// List menu
        /// </summary>
        public static void ListRecords()
        {
            GetEnum();
            CursosPos.Clrscr();

            Console.ForegroundColor = ConsoleColor.Red;
            CursosPos.Gotoxy(10, 4);
            Console.Write("Welcome to Rent-A-Car 0.4!");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            CursosPos.Gotoxy(10, 5);
            Console.Write("--------------------------------------------------------------------------------");

            Console.ForegroundColor = ConsoleColor.Red;
            CursosPos.Gotoxy(10, 6);
            if (enu.Equals(ProjectTools.TableTypes.COMPANY))
            {
                Console.Write("ID  NAME               COUNTRY        FOUNDATION YEARLY INCOME  URL");
            }

            if (enu.Equals(ProjectTools.TableTypes.EXTRA))
            {
                Console.Write("ID  NAME                         PRICE COLOUR   MULTIPLE USE");
            }

            if (enu.Equals(ProjectTools.TableTypes.MODELL))
            {
                Console.Write("ID  NAME               RELEASE ENGINE  POWER  PRICE");
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            CursosPos.Gotoxy(10, 7);

            int pos = 8;

            if (enu.Equals(ProjectTools.TableTypes.COMPANY))
            {
                List<COMPANY> cOMPANYList = bCL.List().ToList();

                // Enable Paging
                for (int i = 0; i < cOMPANYList.Count(); i++)
                {
                    CursosPos.Gotoxy(10, pos);
                    Console.Write(cOMPANYList[i].ID);
                    CursosPos.Gotoxy(14, pos);
                    Console.Write(cOMPANYList[i].NAME);
                    CursosPos.Gotoxy(33, pos);
                    Console.Write(cOMPANYList[i].COUNTRY);
                    CursosPos.Gotoxy(48, pos);
                    Console.Write(cOMPANYList[i].FOUNDATION);
                    CursosPos.Gotoxy(59, pos);
                    Console.Write(cOMPANYList[i].YEARLY_INCOME);
                    CursosPos.Gotoxy(74, pos);
                    Console.Write(cOMPANYList[i].URL);
                    pos++;
                }
            }

            if (enu.Equals(ProjectTools.TableTypes.EXTRA))
            {
                var eXTRAList = bEL.List().ToList();
                var temp = bEL.ConnectList().ToList();
                var mODELLList = bML.List().ToList();

                // Enable Paging
                for (int i = 0; i < eXTRAList.Count(); i++)
                {
                    CursosPos.Gotoxy(10, pos);
                    Console.Write(eXTRAList[i].ID);
                    CursosPos.Gotoxy(14, pos);
                    Console.Write(eXTRAList[i].NAME);
                    CursosPos.Gotoxy(43, pos);
                    Console.Write(eXTRAList[i].PRICE);
                    CursosPos.Gotoxy(49, pos);
                    Console.Write(eXTRAList[i].COLOR);
                    CursosPos.Gotoxy(58, pos);
                    Console.Write(eXTRAList[i].MULTIPLE_USE);
                    /* Unused feature
                    var extrabelongsomodell = from y in mODELLList where y.ID.Equals((from x in temp where x.EXTRA_ID.Equals(eXTRAList[i].ID) select x.MODELL_ID).First()) select y.NAME; // Console.Write(eXTRAList[i].ID);
                    if (extrabelongsomodell.Count() > 0)
                    {
                        CursosPos.Gotoxy(66, pos);
                        Console.Write(extrabelongsomodell.First());
                    }
                    */
                    pos++;
                }
            }

            if (enu.Equals(ProjectTools.TableTypes.MODELL))
            {
                List<MODELL> mODELLList = bML.List().ToList();

                // Enable Paging
                for (int i = 0; i < mODELLList.Count(); i++)
                {
                    CursosPos.Gotoxy(10, pos);
                    Console.Write(mODELLList[i].ID);
                    CursosPos.Gotoxy(14, pos);
                    Console.Write(mODELLList[i].NAME);
                    CursosPos.Gotoxy(34, pos);
                    Console.Write(mODELLList[i].RELEASE);
                    CursosPos.Gotoxy(41, pos);
                    Console.Write(mODELLList[i].ENGINE);
                    CursosPos.Gotoxy(49, pos);
                    Console.Write(mODELLList[i].POWER);
                    CursosPos.Gotoxy(56, pos);
                    Console.Write(mODELLList[i].PRICE);
                    pos++;
                }
            }

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            CursosPos.Gotoxy(10, pos++);
            Console.Write("--------------------------------------------------------------------------------");
            pos++;
            CursosPos.Gotoxy(10, pos++);
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Add menu
        /// </summary>
        public static void AddRecord()
        {
            try
            {
                GetEnum();
                while (true)
                {
                    CursosPos.Clrscr();

                    if (enu.Equals(ProjectTools.TableTypes.COMPANY))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        CursosPos.Gotoxy(10, 4);
                        Console.Write("Welcome to Rent-A-Car 0.4!");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 5);
                        Console.Write("-------------------------------------------");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        CursosPos.Gotoxy(10, 6);
                        Console.Write("Company Name: ");

                        CursosPos.Gotoxy(10, 7);
                        Console.Write("Country: ");

                        CursosPos.Gotoxy(10, 8);
                        Console.Write("URL: ");

                        CursosPos.Gotoxy(10, 9);
                        Console.Write("Foundation: ");

                        CursosPos.Gotoxy(10, 10);
                        Console.Write("Yearly income: ");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 11);
                        Console.Write("-------------------------------------------");

                        COMPANY temp = new COMPANY();

                        CursosPos.Gotoxy(28, 6);
                        string nameof = Console.ReadLine();
                        temp.NAME = nameof;

                        CursosPos.Gotoxy(28, 7);
                        temp.COUNTRY = Console.ReadLine();

                        CursosPos.Gotoxy(28, 8);
                        temp.URL = Console.ReadLine();

                        CursosPos.Gotoxy(28, 9);
                        temp.FOUNDATION = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 10);
                        temp.YEARLY_INCOME = Convert.ToInt32(Console.ReadLine());

                        bCL.Add(temp, nameof);

                        Console.ForegroundColor = ConsoleColor.Red;
                        CursosPos.Gotoxy(10, 13);
                        Console.Write("Do you want to add another record (Y/N)? ");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        char ch = Convert.ToChar(Console.ReadLine());

                        Console.ForegroundColor = ConsoleColor.White;
                        if (ch == 'Y' || ch == 'y')
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (enu.Equals(ProjectTools.TableTypes.EXTRA))
                    {
                        CursosPos.Gotoxy(10, 4);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Welcome to Rent-A-Car 0.4!");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 5);
                        Console.Write("-------------------------------------------");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        CursosPos.Gotoxy(10, 6);
                        Console.Write("Extra Name: ");

                        CursosPos.Gotoxy(10, 7);
                        Console.Write("Price: ");

                        CursosPos.Gotoxy(10, 8);
                        Console.Write("Color: ");

                        CursosPos.Gotoxy(10, 9);
                        Console.Write("Multiple use: (Y/N) ");

                        CursosPos.Gotoxy(10, 10);
                        Console.Write("Modell name: ");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 11);
                        Console.Write("-------------------------------------------");

                        EXTRA temp = new EXTRA();

                        CursosPos.Gotoxy(28, 6);
                        temp.NAME = Console.ReadLine();

                        CursosPos.Gotoxy(28, 7);
                        temp.PRICE = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 8);
                        temp.COLOR = Console.ReadLine();

                        CursosPos.Gotoxy(28, 9);
                        temp.MULTIPLE_USE = Console.ReadLine().ToUpper().StartsWith("Y") ? true : false;

                        CursosPos.Gotoxy(28, 10);
                        string modell_name = Console.ReadLine();

                        bEL.Add(temp, modell_name);

                        CursosPos.Gotoxy(10, 13);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Do you want to add another record (Y/N)? ");
                        char ch = Convert.ToChar(Console.ReadLine());

                        Console.ForegroundColor = ConsoleColor.White;
                        if (ch == 'Y' || ch == 'y')
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (enu.Equals(ProjectTools.TableTypes.MODELL))
                    {
                        CursosPos.Gotoxy(10, 4);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Welcome to Rent-A-Car 0.4!");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 5);
                        Console.Write("-------------------------------------------");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        CursosPos.Gotoxy(10, 6);
                        Console.Write("Modell Name: ");

                        CursosPos.Gotoxy(10, 7);
                        Console.Write("Release: ");

                        CursosPos.Gotoxy(10, 8);
                        Console.Write("Engine: ");

                        CursosPos.Gotoxy(10, 9);
                        Console.Write("Power: ");

                        CursosPos.Gotoxy(10, 10);
                        Console.Write("Price: ");

                        CursosPos.Gotoxy(10, 11);
                        Console.Write("Company name: ");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 12);
                        Console.Write("-------------------------------------------");

                        MODELL temp = new MODELL();

                        CursosPos.Gotoxy(28, 6);
                        temp.NAME = Console.ReadLine();

                        CursosPos.Gotoxy(28, 7);
                        temp.RELEASE = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 8);
                        temp.ENGINE = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 9);
                        temp.POWER = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 10);
                        temp.PRICE = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 11);
                        string nameof = Console.ReadLine();

                        bML.Add(temp, nameof);

                        Console.ForegroundColor = ConsoleColor.Red;
                        CursosPos.Gotoxy(10, 14);
                        Console.Write("Do you want to add another record (Y/N)? ");
                        char ch = Convert.ToChar(Console.ReadLine());

                        Console.ForegroundColor = ConsoleColor.White;
                        if (ch == 'Y' || ch == 'y')
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CursosPos.Clrscr();
                CursosPos.Gotoxy(10, 0);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Update menu
        /// </summary>
        public static void UpdateRecord()
        {
            try
            {
                ListRecords();
                CursosPos.Gotoxy(10, 4);

                Console.ReadKey();
                while (true)
                {
                    CursosPos.Clrscr();

                    if (enu.Equals(ProjectTools.TableTypes.COMPANY))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        CursosPos.Gotoxy(10, 4);
                        Console.Write("Welcome to Rent-A-Car 0.4!");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 5);
                        Console.Write("-------------------------------------------");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        CursosPos.Gotoxy(10, 6);
                        Console.Write("Company Name: ");

                        CursosPos.Gotoxy(10, 7);
                        Console.Write("Country: ");

                        CursosPos.Gotoxy(10, 8);
                        Console.Write("URL: ");

                        CursosPos.Gotoxy(10, 9);
                        Console.Write("Foundation: ");

                        CursosPos.Gotoxy(10, 10);
                        Console.Write("Yearly income: ");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 11);
                        Console.Write("-------------------------------------------");

                        COMPANY temp = new COMPANY();

                        CursosPos.Gotoxy(28, 6);
                        string nameof = Console.ReadLine();
                        temp.NAME = nameof;

                        CursosPos.Gotoxy(28, 7);
                        temp.COUNTRY = Console.ReadLine();

                        CursosPos.Gotoxy(28, 8);
                        temp.URL = Console.ReadLine();

                        CursosPos.Gotoxy(28, 9);
                        temp.FOUNDATION = Convert.ToInt32(Console.ReadLine());

                        bCL.Update(temp);

                        CursosPos.Gotoxy(10, 12);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("Do you want to update another record (Y/N)? ");
                        char ch = Convert.ToChar(Console.ReadLine());

                        if (ch == 'Y' || ch == 'y')
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (enu.Equals(ProjectTools.TableTypes.EXTRA))
                    {
                        CursosPos.Gotoxy(10, 4);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Welcome to Rent-A-Car 0.4!");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 5);
                        Console.Write("-------------------------------------------");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        CursosPos.Gotoxy(10, 6);
                        Console.Write("Extra Name: ");

                        CursosPos.Gotoxy(10, 7);
                        Console.Write("Price: ");

                        CursosPos.Gotoxy(10, 8);
                        Console.Write("Color: ");

                        CursosPos.Gotoxy(10, 9);
                        Console.Write("Multiple use: (Y/N) ");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 10);
                        Console.Write("-------------------------------------------");

                        EXTRA temp = new EXTRA();

                        CursosPos.Gotoxy(28, 6);
                        temp.NAME = Console.ReadLine();

                        CursosPos.Gotoxy(28, 7);
                        temp.PRICE = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 8);
                        temp.COLOR = Console.ReadLine();

                        CursosPos.Gotoxy(28, 9);
                        temp.MULTIPLE_USE = Console.ReadLine().StartsWith("Y") ? true : false;

                        bEL.Update(temp);

                        CursosPos.Gotoxy(10, 12);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Do you want to update another record (Y/N)? ");
                        char ch = Convert.ToChar(Console.ReadLine());

                        Console.ForegroundColor = ConsoleColor.White;
                        if (ch == 'Y' || ch == 'y')
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (enu.Equals(ProjectTools.TableTypes.MODELL))
                    {
                        CursosPos.Gotoxy(10, 4);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Welcome to Rent-A-Car 0.4!");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 5);
                        Console.Write("-------------------------------------------");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        CursosPos.Gotoxy(10, 6);
                        Console.Write("Modell Name: ");

                        CursosPos.Gotoxy(10, 7);
                        Console.Write("Release: ");

                        CursosPos.Gotoxy(10, 8);
                        Console.Write("Engine: ");

                        CursosPos.Gotoxy(10, 9);
                        Console.Write("Power: ");

                        CursosPos.Gotoxy(10, 10);
                        Console.Write("Price: ");

                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        CursosPos.Gotoxy(10, 11);
                        Console.Write("-------------------------------------------");

                        MODELL temp = new MODELL();

                        CursosPos.Gotoxy(28, 6);
                        temp.NAME = Console.ReadLine();

                        CursosPos.Gotoxy(28, 7);
                        temp.RELEASE = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 8);
                        temp.ENGINE = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 9);
                        temp.POWER = Convert.ToInt32(Console.ReadLine());

                        CursosPos.Gotoxy(28, 10);
                        temp.PRICE = Convert.ToInt32(Console.ReadLine());

                        bML.Update(temp);

                        Console.ForegroundColor = ConsoleColor.Red;
                        CursosPos.Gotoxy(10, 13);
                        Console.Write("Do you want to update another record (Y/N)? ");
                        char ch = Convert.ToChar(Console.ReadLine());

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        if (ch == 'Y' || ch == 'y')
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CursosPos.Clrscr();
                CursosPos.Gotoxy(10, 4);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Delete a row
        /// </summary>
        public static void DeleteRecord()
        {
            ListRecords();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Enter the name of the item you want to delete: ");
            string nameof;
            Console.ForegroundColor = ConsoleColor.Yellow;
            nameof = Console.ReadLine();
            try
            {
                if (enu.Equals(ProjectTools.TableTypes.COMPANY))
                {
                    bCL.Delete(nameof);
                }

                if (enu.Equals(ProjectTools.TableTypes.EXTRA))
                {
                    bEL.Delete(nameof);
                }

                if (enu.Equals(ProjectTools.TableTypes.MODELL))
                {
                    bML.Delete(nameof);
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(nameof + " deleted!");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                CursosPos.Clrscr();
                CursosPos.Gotoxy(0, 5);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadKey();
            }
        }

        /// <summary>
        /// writes out the ModellFullpriceRecord non_CRUD method
        /// </summary>
        public static void ModellFullpriceRecord()
        {
            enu2 = ProjectTools.NonCRUDTypes.ModellFullprice;
            CursosPos.Clrscr();
            ListRecordsDictionary(bNCL.ModellFullprice());
        }

        /// <summary>
        /// writes out the CompanyAveragePriceRecord non_CRUD method
        /// </summary>
        public static void CompanyAveragePriceRecord()
        {
            // BNCL
            enu2 = ProjectTools.NonCRUDTypes.CompanyAveragePrice;
            CursosPos.Clrscr();

            ListRecordsDictionary(bNCL.CompanyAveragePrice());
        }

        /// <summary>
        /// writes out the ExtraxNumberByModellsRecord non_CRUD method
        /// </summary>
        public static void ExtraxNumberByModellsRecord()
        {
            // BNCL
            enu2 = ProjectTools.NonCRUDTypes.ExtraxDifferentColoursByModells;
            CursosPos.Clrscr();

            ListRecordsDictionary(bNCL.ExtraxDifferentColoursByModells());
        }

        /// <summary>
        /// List viewer menu for non-CRUD functions
        /// </summary>
        /// <param name="beof">The dictionary of results</param>
        public static void ListRecordsDictionary(Dictionary<string, int> beof)
        {
            try
            {
                CursosPos.Clrscr();

                CursosPos.Gotoxy(10, 4);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Welcome to Rent-A-Car 0.4!");

                CursosPos.Gotoxy(10, 5);
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.Write("--------------------------------------------------------------------------------");

                Console.ForegroundColor = ConsoleColor.Red;
                CursosPos.Gotoxy(10, 6);
                Console.Write("Item                 Value");
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                CursosPos.Gotoxy(10, 7);

                Console.Write("--------------------------------------------------------------------------------");
                int pos = 8;

                Console.ForegroundColor = ConsoleColor.Yellow;

                // Enable Paging
                foreach (var x in beof)
                {
                    CursosPos.Gotoxy(10, pos);
                    Console.Write(x.Key);
                    CursosPos.Gotoxy(31, pos);
                    Console.Write(x.Value);
                    pos++;
                }

                Console.ForegroundColor = ConsoleColor.DarkCyan;
                CursosPos.Gotoxy(10, pos++);
                Console.Write("--------------------------------------------------------------------------------");
                pos++;
                CursosPos.Gotoxy(10, pos++);
                Console.ForegroundColor = ConsoleColor.White;
            }
            catch (Exception e)
            {
                CursosPos.Clrscr();
                CursosPos.Gotoxy(10, 4);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Menu display, only the text
        /// </summary>
        /// <returns>Int: The users choice of function</returns>
        public static int DisplayMainMenu()
        {
            CursosPos.Clrscr();
            CursosPos.Gotoxy(10, 4);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Welcome to Rent-A-Car 0.4!");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            CursosPos.Gotoxy(10, 5);
            Console.Write("-------------------------------------------");

            Console.ForegroundColor = ConsoleColor.Yellow;
            CursosPos.Gotoxy(10, 6);
            Console.Write("1. Add new Modell/Company/Extra");

            CursosPos.Gotoxy(10, 7);
            Console.Write("2. Update existing Modell/Company/Extra");

            CursosPos.Gotoxy(10, 8);
            Console.Write("3. View List of Modell/Company/Extra");

            CursosPos.Gotoxy(10, 9);
            Console.Write("4. Delete existing Modell/Company/Extra");

            CursosPos.Gotoxy(10, 10);
            Console.Write("5. List all modells with it's full price");

            CursosPos.Gotoxy(10, 11);
            Console.Write("6. List all companies with average modell prices");

            CursosPos.Gotoxy(10, 12);
            Console.Write("7. List number of extras on all models");

            CursosPos.Gotoxy(10, 13);
            Console.Write("8. Exit");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            CursosPos.Gotoxy(10, 14);
            Console.Write("-------------------------------------------");

            Console.ForegroundColor = ConsoleColor.Yellow;
            CursosPos.Gotoxy(10, 15);
            Console.Write("Enter your Selection: ");
            int m = -1;
            m = Convert.ToInt32(Console.ReadLine());

            return m;
        }

        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="args">string[] args</param>
        public static void Main(string[] args)
        {
            bML = new BusinessModellLogic();
            bEL = new BusinessExtraLogic();
            bCL = new BusinessCompanyLogic();
            bNCL = new BusinessNonCRUDLogic();
            Console.ForegroundColor = ConsoleColor.Yellow;
            while (true)
            {
                int selection = DisplayMainMenu();
                switch (selection)
                {
                    case 1:
                        AddRecord();
                        break;
                    case 2:
                        UpdateRecord();
                        break;
                    case 3:
                        {
                            ListRecords();
                            Console.Write("Press any key to return to Main Manu: ");
                            char ch = Console.ReadKey().KeyChar;
                        }

                        break;
                    case 4:
                        DeleteRecord();
                        break;
                    case 5:
                        {
                            ModellFullpriceRecord();
                            Console.Write("Press any key to return to Main Manu: ");
                            char ch = Console.ReadKey().KeyChar;
                        }

                        break;
                    case 6:
                        {
                            CompanyAveragePriceRecord();
                            Console.Write("Press any key to return to Main Manu: ");
                            char ch = Console.ReadKey().KeyChar;
                        }

                        break;
                    case 7:
                        {
                            ExtraxNumberByModellsRecord();
                            Console.Write("Press any key to return to Main Manu: ");
                            char ch = Console.ReadKey().KeyChar;
                        }

                        break;
                    case 8:
                        return;
                    default:
                        break;
                }

                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}