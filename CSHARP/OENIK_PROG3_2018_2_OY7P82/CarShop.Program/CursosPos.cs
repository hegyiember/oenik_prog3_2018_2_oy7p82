﻿// <copyright file="CursosPos.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using CarShop.Data;
    using CarShop.Logic;
    using CarShop.Tools;

    /// <summary>
    /// Cursonpositioning class, sets the cursor to the given coordinates
    /// </summary>
    public class CursosPos
    {
        /// <summary>
        /// Clear the console area
        /// </summary>
        public static void Clrscr()
        {
            Console.Clear();
            int stdoutha = -11;
            POSITION pos;
            pos.X = pos.Y = 0;
            int writtenbytes = 0;
            FillConsoleOutputCharacter(GetStdHandle(stdoutha), 32, 80 * 25, pos, ref writtenbytes);
        }

        /// <summary>
        /// Sets the cursor to the given coorditate
        /// </summary>
        /// <param name="x">X coorditane</param>
        /// <param name="y">Y coorditane</param>
        public static void Gotoxy(int x, int y)
        {
            int stdoutha = -11;
            POSITION pos;
            pos.X = (short)x;
            pos.Y = (short)y;
            SetConsoleCursorPosition(GetStdHandle(stdoutha), pos);
        }

        /// <summary>
        /// Sets the cursor to the given coorditate
        /// </summary>
        /// <param name="x">X coorditane</param>
        /// <param name="y">Y coorditane</param>
        public static void Gotoxy(short x, short y)
        {
            int stdoutha = -11;
            POSITION pos;
            pos.X = x;
            pos.Y = y;
            SetConsoleCursorPosition(GetStdHandle(stdoutha), pos);
        }

        [DllImport("kernel32.dll", EntryPoint = "GetStdHandle", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern int GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll", EntryPoint = "SetConsoleCursorPosition", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern int SetConsoleCursorPosition(int hConsoleOutput, POSITION dwCursorPosition);

        [DllImport("kernel32.dll", EntryPoint = "FillConsoleOutputCharacter", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern int FillConsoleOutputCharacter(int hConsoleOutput, byte cCharacter, int nLength, POSITION dwWriteCoord, ref int lpNumberOfCharsWritten);

        /// <summary>
        /// Container of a coorditae
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct POSITION
        {
            /// <summary>
            /// X coorditane
            /// </summary>
            public short X;

            /// <summary>
            /// Y coorditane
            /// </summary>
            public short Y;
        }
    }
}