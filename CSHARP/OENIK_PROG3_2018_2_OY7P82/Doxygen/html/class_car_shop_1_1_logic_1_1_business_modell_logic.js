var class_car_shop_1_1_logic_1_1_business_modell_logic =
[
    [ "BusinessModellLogic", "class_car_shop_1_1_logic_1_1_business_modell_logic.html#a4b49aedc519622aa271cae831a2fc8a7", null ],
    [ "BusinessModellLogic", "class_car_shop_1_1_logic_1_1_business_modell_logic.html#ac4446468bbb1cef93514941eac5bc623", null ],
    [ "Add", "class_car_shop_1_1_logic_1_1_business_modell_logic.html#a222ec9ab8a32c60ff1bfd23ddda55a96", null ],
    [ "Delete", "class_car_shop_1_1_logic_1_1_business_modell_logic.html#aac1ea20c45e5c3b89772691b28116ce3", null ],
    [ "FindById", "class_car_shop_1_1_logic_1_1_business_modell_logic.html#a8e29e9021dd4fff8d7b7e1bf1f5d253f", null ],
    [ "List", "class_car_shop_1_1_logic_1_1_business_modell_logic.html#ad966a812ff0cc2528ef1244a97460e5c", null ],
    [ "Update", "class_car_shop_1_1_logic_1_1_business_modell_logic.html#a92ea52ff37dfa37fe1e2c81220e13adb", null ]
];