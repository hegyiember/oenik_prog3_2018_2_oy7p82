var class_car_shop_1_1_repository_1_1_repository_extra_logic =
[
    [ "RepositoryExtraLogic", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html#ad39fa3326f83a508158c010604d4e57b", null ],
    [ "Add", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html#a6748e3d5b75efd0fac51958832fc2c7e", null ],
    [ "Delete", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html#a65c357745729deada9e8422a42285506", null ],
    [ "FindById", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html#ad64853d91accac93369a24d8f31d9913", null ],
    [ "GetModellExtraJoin", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html#a7b8b4490f20794cca0d1444185fafeab", null ],
    [ "Update", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html#a2520d6cc0f9834d176d09126db0a0d4f", null ],
    [ "List", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html#a0f5fcfe1766f4617790e4bd5d4bfd99f", null ]
];