var namespace_car_shop_1_1_repository =
[
    [ "Interfaces", "namespace_car_shop_1_1_repository_1_1_interfaces.html", "namespace_car_shop_1_1_repository_1_1_interfaces" ],
    [ "RepositoryCompanyLogic", "class_car_shop_1_1_repository_1_1_repository_company_logic.html", "class_car_shop_1_1_repository_1_1_repository_company_logic" ],
    [ "RepositoryExtraLogic", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html", "class_car_shop_1_1_repository_1_1_repository_extra_logic" ],
    [ "RepositoryModellLogic", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html", "class_car_shop_1_1_repository_1_1_repository_modell_logic" ]
];