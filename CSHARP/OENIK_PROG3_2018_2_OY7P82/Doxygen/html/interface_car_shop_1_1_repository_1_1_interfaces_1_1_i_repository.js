var interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository =
[
    [ "Add", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#a72c43a8d8194d2b4f2b9a22f6e54536e", null ],
    [ "Delete", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#a56b3e0f7fceef68a2a64be62f5575980", null ],
    [ "FindById", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#a83e9cb063503a79bcdbfac81c94f9cea", null ],
    [ "GetModellExtraJoin", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#aeadd9e27574b95f714c32b75d913a8c8", null ],
    [ "Update", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#a99758307e348da36b0171428f8b2e315", null ],
    [ "List", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#ae49a9aca1567e90ad8338ea23425b53a", null ]
];