var dir_b4d87d72b9058f36de88c611a50d0413 =
[
    [ "CarShop.Data", "dir_9f3bb1ee717b354237688a3a7b99d028.html", "dir_9f3bb1ee717b354237688a3a7b99d028" ],
    [ "CarShop.Logic", "dir_f617fbb001dca73a6024139cf8c38ac3.html", "dir_f617fbb001dca73a6024139cf8c38ac3" ],
    [ "CarShop.Logic.Tests", "dir_3ed9bd5a4e3f95694fb7488cea2ae51f.html", "dir_3ed9bd5a4e3f95694fb7488cea2ae51f" ],
    [ "CarShop.Program", "dir_2d4b55b06b86a0367130bdbf7ef91b7a.html", "dir_2d4b55b06b86a0367130bdbf7ef91b7a" ],
    [ "CarShop.Repository", "dir_5aefa8c9b80cbd76b7abe1ad8335d83b.html", "dir_5aefa8c9b80cbd76b7abe1ad8335d83b" ],
    [ "CarShop.Tools", "dir_13a2c0f72dd616df69e77d262442e0f7.html", "dir_13a2c0f72dd616df69e77d262442e0f7" ]
];