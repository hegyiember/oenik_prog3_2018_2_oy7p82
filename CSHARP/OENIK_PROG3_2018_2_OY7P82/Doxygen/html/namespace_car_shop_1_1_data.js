var namespace_car_shop_1_1_data =
[
    [ "COMPANY", "class_car_shop_1_1_data_1_1_c_o_m_p_a_n_y.html", "class_car_shop_1_1_data_1_1_c_o_m_p_a_n_y" ],
    [ "DataBaseProviderFactory", "class_car_shop_1_1_data_1_1_data_base_provider_factory.html", "class_car_shop_1_1_data_1_1_data_base_provider_factory" ],
    [ "DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", "class_car_shop_1_1_data_1_1_d_b_entities" ],
    [ "EXTRA", "class_car_shop_1_1_data_1_1_e_x_t_r_a.html", "class_car_shop_1_1_data_1_1_e_x_t_r_a" ],
    [ "MODELL", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l.html", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l" ],
    [ "MODELL_EXTRA_CONNECT", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l___e_x_t_r_a___c_o_n_n_e_c_t.html", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l___e_x_t_r_a___c_o_n_n_e_c_t" ]
];