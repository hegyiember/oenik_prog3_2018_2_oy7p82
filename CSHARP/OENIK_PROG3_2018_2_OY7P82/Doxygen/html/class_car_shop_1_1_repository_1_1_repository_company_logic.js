var class_car_shop_1_1_repository_1_1_repository_company_logic =
[
    [ "RepositoryCompanyLogic", "class_car_shop_1_1_repository_1_1_repository_company_logic.html#a027c34bf7f054dc0df8076839e9308ae", null ],
    [ "Add", "class_car_shop_1_1_repository_1_1_repository_company_logic.html#ad66dce55d7e7569e3cba43c1c85b1cda", null ],
    [ "Delete", "class_car_shop_1_1_repository_1_1_repository_company_logic.html#a442e14f97ce24043ca2a0ca78fc00b4c", null ],
    [ "FindById", "class_car_shop_1_1_repository_1_1_repository_company_logic.html#a23ba66b8bda1a46f65a1bec6a3516d06", null ],
    [ "GetModellExtraJoin", "class_car_shop_1_1_repository_1_1_repository_company_logic.html#aa8c5eaa2f40b2aacbc4e129f4da06208", null ],
    [ "Update", "class_car_shop_1_1_repository_1_1_repository_company_logic.html#aabf0436f4975ea78f2d98945354500c2", null ],
    [ "List", "class_car_shop_1_1_repository_1_1_repository_company_logic.html#ae867a9f8085d2dba846f957c69ed56e8", null ]
];