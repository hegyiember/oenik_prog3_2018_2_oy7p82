var class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst =
[
    [ "CompanyAveragePriceTest1", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a0f2d417aeb83f8c8839ea6e310305b4a", null ],
    [ "CompanyAveragePriceTest2", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#afe48744da9e6c6d3141a08cee318d921", null ],
    [ "ExtraxDifferentColoursByModellsTest1", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#ac4a8dd6e77bbd9a1a994c89d064e85f2", null ],
    [ "ExtraxDifferentColoursByModellsTest2", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#aa746acec0632827f399d6d2322939668", null ],
    [ "ModellFullpricetest1", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a9816efef9d89b17711483e337aefeccc", null ],
    [ "Setup", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a08df6c44a5c8d510d948a2fab3d7c7be", null ],
    [ "WhenInsertCOMPANYInBusinessLogicIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a60444def3ae09c4c7204c2a1ce866a45", null ],
    [ "WhenInsertCOMPANYInBusinessLogicIsCalled_TheRepoVersionIsCalledToo_ExceptionNo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#ae431160f01f71d5b42637084dc608ff1", null ],
    [ "WhenInsertEXTRAInBusinessLogicIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a72764f9e481718738343ada830e35c33", null ],
    [ "WhenInsertEXTRAInBusinessLogicIsCalled_TheRepoVersionIsCalledToo_ExceptionNo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#adeef1f2103985ebe74f8d17a67bba791", null ],
    [ "WhenInsertMODELLInBusinessLogicIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a50b37a9df82fd4f99e605abe8e4134ba", null ],
    [ "WhenInsertMODELLInBusinessLogicIsCalled_TheRepoVersionIsCalledToo_ExceptionNo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a24586cc94b7f0e0eaa90d728d4ec07c1", null ],
    [ "WhenUpdateCOMPANYInBusinessLogicIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a9d73dc5232ab9f69704db37d5a0da27f", null ],
    [ "WhenUpdateEXTRAInBusinessLogicIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#afd90a6f2b3a9ae295ae2c5d5c369457e", null ],
    [ "WhenUpdateMODELLInBusinessLogicIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#acfa223bddcb08b457bc74e2d43e2afb4", null ],
    [ "LogicNONCRUD", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#a5348142a12302a88109e4e58f8207711", null ],
    [ "MockingNONCRUD", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html#adad5c8afcf9632b052a7a2a803656422", null ]
];