var indexSectionsWithContent =
{
  0: "abcdefgilmnprstuwxy",
  1: "bcdeilmpr",
  2: "c",
  3: "abcdefglmrsuw",
  4: "lxy",
  5: "nt",
  6: "cemp",
  7: "dlm",
  8: "cn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Pages"
};

