var searchData=
[
  ['ibudinesslogicother',['IBudinessLogicOther',['../interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_budiness_logic_other.html',1,'CarShop::Logic::Interfaces']]],
  ['ibusinesslogiccrud',['IBusinessLogicCRUD',['../interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html',1,'CarShop::Logic::Interfaces']]],
  ['ibusinesslogiccrud_3c_20company_20_3e',['IBusinessLogicCRUD&lt; COMPANY &gt;',['../interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html',1,'CarShop::Logic::Interfaces']]],
  ['ibusinesslogiccrud_3c_20extra_20_3e',['IBusinessLogicCRUD&lt; EXTRA &gt;',['../interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html',1,'CarShop::Logic::Interfaces']]],
  ['ibusinesslogiccrud_3c_20modell_20_3e',['IBusinessLogicCRUD&lt; MODELL &gt;',['../interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html',1,'CarShop::Logic::Interfaces']]],
  ['irepository',['IRepository',['../interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'CarShop::Repository::Interfaces']]],
  ['irepository_3c_20carshop_3a_3adata_3a_3acompany_20_3e',['IRepository&lt; CarShop::Data::COMPANY &gt;',['../interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'CarShop::Repository::Interfaces']]],
  ['irepository_3c_20carshop_3a_3adata_3a_3aextra_20_3e',['IRepository&lt; CarShop::Data::EXTRA &gt;',['../interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'CarShop::Repository::Interfaces']]],
  ['irepository_3c_20carshop_3a_3adata_3a_3amodell_20_3e',['IRepository&lt; CarShop::Data::MODELL &gt;',['../interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'CarShop::Repository::Interfaces']]],
  ['irepository_3c_20company_20_3e',['IRepository&lt; COMPANY &gt;',['../interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'CarShop::Repository::Interfaces']]],
  ['irepository_3c_20extra_20_3e',['IRepository&lt; EXTRA &gt;',['../interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'CarShop::Repository::Interfaces']]],
  ['irepository_3c_20modell_20_3e',['IRepository&lt; MODELL &gt;',['../interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'CarShop::Repository::Interfaces']]]
];
