var interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d =
[
    [ "Add", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html#a1239c70400d7ab5bd3a4f770732f35c0", null ],
    [ "Delete", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html#a2d2fb14076a14bc7be628a5bda88e946", null ],
    [ "FindById", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html#a89818c860c87a422af3cbf7be9e1a8ee", null ],
    [ "List", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html#af58b2377f8aefc43d55b98bbe579c702", null ],
    [ "Update", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html#a78e44d0110dbefa78f3eda9a38d3a806", null ]
];