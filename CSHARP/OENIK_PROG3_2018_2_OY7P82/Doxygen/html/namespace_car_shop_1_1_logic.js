var namespace_car_shop_1_1_logic =
[
    [ "Interfaces", "namespace_car_shop_1_1_logic_1_1_interfaces.html", "namespace_car_shop_1_1_logic_1_1_interfaces" ],
    [ "Tests", "namespace_car_shop_1_1_logic_1_1_tests.html", "namespace_car_shop_1_1_logic_1_1_tests" ],
    [ "BusinessCompanyLogic", "class_car_shop_1_1_logic_1_1_business_company_logic.html", "class_car_shop_1_1_logic_1_1_business_company_logic" ],
    [ "BusinessExtraLogic", "class_car_shop_1_1_logic_1_1_business_extra_logic.html", "class_car_shop_1_1_logic_1_1_business_extra_logic" ],
    [ "BusinessModellLogic", "class_car_shop_1_1_logic_1_1_business_modell_logic.html", "class_car_shop_1_1_logic_1_1_business_modell_logic" ],
    [ "BusinessNonCRUDLogic", "class_car_shop_1_1_logic_1_1_business_non_c_r_u_d_logic.html", "class_car_shop_1_1_logic_1_1_business_non_c_r_u_d_logic" ]
];