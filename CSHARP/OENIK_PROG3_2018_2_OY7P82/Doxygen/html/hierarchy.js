var hierarchy =
[
    [ "CarShop.Data.COMPANY", "class_car_shop_1_1_data_1_1_c_o_m_p_a_n_y.html", null ],
    [ "CarShop.Program.CursosPos", "class_car_shop_1_1_program_1_1_cursos_pos.html", null ],
    [ "CarShop.Data.DataBaseProviderFactory", "class_car_shop_1_1_data_1_1_data_base_provider_factory.html", null ],
    [ "DbContext", null, [
      [ "CarShop.Data.DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", null ],
      [ "CarShop.Data.DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", null ],
      [ "CarShop.Data.DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", null ],
      [ "CarShop.Data.DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", null ],
      [ "CarShop.Data.DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", null ],
      [ "CarShop.Data.DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", null ]
    ] ],
    [ "CarShop.Data.EXTRA", "class_car_shop_1_1_data_1_1_e_x_t_r_a.html", null ],
    [ "CarShop.Logic.Interfaces.IBudinessLogicOther", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_budiness_logic_other.html", [
      [ "CarShop.Logic.BusinessNonCRUDLogic", "class_car_shop_1_1_logic_1_1_business_non_c_r_u_d_logic.html", null ]
    ] ],
    [ "CarShop.Logic.Interfaces.IBusinessLogicCRUD< T >", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html", null ],
    [ "CarShop.Logic.Interfaces.IBusinessLogicCRUD< COMPANY >", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html", [
      [ "CarShop.Logic.BusinessCompanyLogic", "class_car_shop_1_1_logic_1_1_business_company_logic.html", null ]
    ] ],
    [ "CarShop.Logic.Interfaces.IBusinessLogicCRUD< EXTRA >", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html", [
      [ "CarShop.Logic.BusinessExtraLogic", "class_car_shop_1_1_logic_1_1_business_extra_logic.html", null ]
    ] ],
    [ "CarShop.Logic.Interfaces.IBusinessLogicCRUD< MODELL >", "interface_car_shop_1_1_logic_1_1_interfaces_1_1_i_business_logic_c_r_u_d.html", [
      [ "CarShop.Logic.BusinessModellLogic", "class_car_shop_1_1_logic_1_1_business_modell_logic.html", null ]
    ] ],
    [ "CarShop.Repository.Interfaces.IRepository< T >", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "CarShop.Repository.Interfaces.IRepository< CarShop.Data.COMPANY >", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "CarShop.Repository.Interfaces.IRepository< CarShop.Data.EXTRA >", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "CarShop.Repository.Interfaces.IRepository< CarShop.Data.MODELL >", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "CarShop.Repository.Interfaces.IRepository< COMPANY >", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "CarShop.Repository.RepositoryCompanyLogic", "class_car_shop_1_1_repository_1_1_repository_company_logic.html", null ]
    ] ],
    [ "CarShop.Repository.Interfaces.IRepository< EXTRA >", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "CarShop.Repository.RepositoryExtraLogic", "class_car_shop_1_1_repository_1_1_repository_extra_logic.html", null ]
    ] ],
    [ "CarShop.Repository.Interfaces.IRepository< MODELL >", "interface_car_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "CarShop.Repository.RepositoryModellLogic", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html", null ]
    ] ],
    [ "CarShop.Logic.Tests.LogicTetst", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tetst.html", null ],
    [ "CarShop.Data.MODELL", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l.html", null ],
    [ "CarShop.Data.MODELL_EXTRA_CONNECT", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l___e_x_t_r_a___c_o_n_n_e_c_t.html", null ],
    [ "CarShop.Program.CursosPos.POSITION", "struct_car_shop_1_1_program_1_1_cursos_pos_1_1_p_o_s_i_t_i_o_n.html", null ],
    [ "CarShop.Program.Program", "class_car_shop_1_1_program_1_1_program.html", null ],
    [ "CarShop.Tools.ProjectTools", "class_car_shop_1_1_tools_1_1_project_tools.html", null ]
];