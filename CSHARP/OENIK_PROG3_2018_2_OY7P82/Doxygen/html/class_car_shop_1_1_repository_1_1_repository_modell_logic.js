var class_car_shop_1_1_repository_1_1_repository_modell_logic =
[
    [ "RepositoryModellLogic", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html#a7d2fa800af45aa80d2ed53d2b5cec1e9", null ],
    [ "Add", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html#a0164dcc16dfb7d42ddba3cfc0ae72392", null ],
    [ "Delete", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html#aa28f2b8e2f454f9209a04ee9420fcc90", null ],
    [ "FindById", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html#ab4512c4d4a3eb7b1bf6f1fae1e5b6b41", null ],
    [ "GetModellExtraJoin", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html#ab5588f4a4b97b1f56c5cabb83e28e1af", null ],
    [ "Update", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html#a948804d4972955b01bad03c3005ed617", null ],
    [ "List", "class_car_shop_1_1_repository_1_1_repository_modell_logic.html#ada85d0dcbd90bc07d5dd2ccb34defba6", null ]
];