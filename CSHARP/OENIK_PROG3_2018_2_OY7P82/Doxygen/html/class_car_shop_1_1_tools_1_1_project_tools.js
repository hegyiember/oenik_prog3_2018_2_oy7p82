var class_car_shop_1_1_tools_1_1_project_tools =
[
    [ "NonCRUDTypes", "class_car_shop_1_1_tools_1_1_project_tools.html#a562ac80dee109a0a134a0697f10a7ad9", [
      [ "ModellFullprice", "class_car_shop_1_1_tools_1_1_project_tools.html#a562ac80dee109a0a134a0697f10a7ad9a9cd83daf872360d335145e4f7c57279f", null ],
      [ "CompanyAveragePrice", "class_car_shop_1_1_tools_1_1_project_tools.html#a562ac80dee109a0a134a0697f10a7ad9a789dc785f5d80d4309801ab39c2a396b", null ],
      [ "ExtraxDifferentColoursByModells", "class_car_shop_1_1_tools_1_1_project_tools.html#a562ac80dee109a0a134a0697f10a7ad9a1011b676b67ae9377b6e266f6a7a7ce8", null ],
      [ "PLACEHOLDER", "class_car_shop_1_1_tools_1_1_project_tools.html#a562ac80dee109a0a134a0697f10a7ad9ae54e6f6ba0c7cbb4eb7a2016e2f17842", null ]
    ] ],
    [ "TableTypes", "class_car_shop_1_1_tools_1_1_project_tools.html#a9fad9dc0caffbb749a5f6a494d400193", [
      [ "MODELL", "class_car_shop_1_1_tools_1_1_project_tools.html#a9fad9dc0caffbb749a5f6a494d400193a430a9a25de4beeafb92d83fdb26f04f9", null ],
      [ "EXTRA", "class_car_shop_1_1_tools_1_1_project_tools.html#a9fad9dc0caffbb749a5f6a494d400193aa21f6af4d39dbd2643527651dd4e694b", null ],
      [ "COMPANY", "class_car_shop_1_1_tools_1_1_project_tools.html#a9fad9dc0caffbb749a5f6a494d400193a0f025176fb8b953690ad8f20f119ed75", null ],
      [ "PLACEHOLDER", "class_car_shop_1_1_tools_1_1_project_tools.html#a9fad9dc0caffbb749a5f6a494d400193ae54e6f6ba0c7cbb4eb7a2016e2f17842", null ]
    ] ]
];