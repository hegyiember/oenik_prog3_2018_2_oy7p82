﻿// <copyright file="RepositoryCompanyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository.Interfaces;

    /// <summary>
    /// Repostory of the Company table
    /// </summary>
    public class RepositoryCompanyLogic : IRepository<COMPANY>
    {
        private readonly DBEntities dB;
        private int addnumberfromdelete;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryCompanyLogic"/> class.
        /// asdads
        /// </summary>
        public RepositoryCompanyLogic()
        {
            this.dB = new DataBaseProviderFactory().DB;
            this.addnumberfromdelete = 0;
        }

        /// <summary>
        /// Gets sdas
        /// </summary>
        public IEnumerable<COMPANY> List => this.dB.COMPANies.ToList();

        /// <summary>
        /// Returns list of connect table MODELL_EXTRA
        /// </summary>
        /// <returns>List of MODELL_EXTRA</returns>
        public List<MODELL_EXTRA_CONNECT> GetModellExtraJoin()
        {
            return this.dB.MODELL_EXTRA_CONNECT.ToList();
        }

        /// <summary>
        /// Find element by id
        /// </summary>
        /// <param name="id">Element of id to be find</param>
        /// <returns>Element</returns>
        public COMPANY FindById(int id)
        {
            var temp = from x in this.dB.COMPANies where x.ID == id select x;
            if (temp.Count() == 0 || temp == null)
            {
                return new COMPANY { ID = -1 };
            }

            return temp.First();
        }

        /// <summary>
        /// Add a new element into Company table
        /// </summary>
        /// <param name="be">New element</param>
        /// <param name="cOMPANY_NAME">Company's name</param>
        public void Add(COMPANY be, string cOMPANY_NAME)
        {
            var distinct = from x in this.dB.COMPANies where x.NAME.Equals(be.NAME) select x;
            if (distinct.Count() > 0)
            {
                throw new Exception("There already is a company under the name of " + cOMPANY_NAME);
            }

            decimal temp = this.dB.COMPANies.Count() == 0 ? 1 : this.dB.COMPANies.Count() + 1 + this.addnumberfromdelete;
            this.dB.COMPANies.Add(new COMPANY
            {
                ID = temp,
                NAME = be.NAME,
                COUNTRY = be.COUNTRY,
                URL = be.URL,
                FOUNDATION = be.FOUNDATION,
                YEARLY_INCOME = be.YEARLY_INCOME
            });
            this.dB.SaveChanges();
        }

        /// <summary>
        /// Delete a row by NAME
        /// </summary>
        /// <param name="nAME">The NAME value of the deleting instance</param>
        public void Delete(string nAME)
        {
            var temp = from x in this.dB.COMPANies where x.NAME.Equals(nAME) select x;
            if (temp.Count() == 0)
            {
                throw new Exception("Item not found!");
            }

            var kek = temp.First();
            var temp2 = from x in this.dB.MODELLs where x.COMPANY_ID.Equals(kek.ID) select x;
            if (temp2.Count() == 0)
            {

                this.dB.COMPANies.Remove(kek);
                this.addnumberfromdelete++;
                this.dB.SaveChanges();
                throw new Exception("No modell belongs to the company");
            }

            foreach (var x in temp2)
            {
                var temp3 = from y in this.dB.MODELL_EXTRA_CONNECT where y.MODELL_ID.Equals(x.ID) select y;
                if (temp3.Count() != 0)
                {
                    foreach (var y in temp3)
                    {
                        this.dB.MODELL_EXTRA_CONNECT.Remove(y);
                    }
                }
                this.dB.MODELLs.Remove(x);
            }

            this.dB.COMPANies.Remove(kek);
            this.addnumberfromdelete++;
            this.dB.SaveChanges();
        }

        /// <summary>
        /// Update a row
        /// </summary>
        /// <param name="entity">Element to be updated</param>
        public void Update(COMPANY entity)
        {
            var check = from x in this.dB.COMPANies where x.NAME.Equals(entity.NAME) select x;
            if (check.Count() == 1)
            {
                var refer = this.dB.COMPANies.SingleOrDefault(b => b.NAME.Equals(entity.NAME));
                if (refer != null)
                {
                    refer.NAME = entity.NAME;
                    refer.COUNTRY = entity.COUNTRY;
                    refer.URL = entity.URL;
                    refer.FOUNDATION = entity.FOUNDATION;
                    refer.YEARLY_INCOME = entity.YEARLY_INCOME;
                    this.dB.SaveChanges();
                }
            }
            else if (check.Count() > 1)
            {
                throw new Exception("There are more than one of the " + entity.NAME);
            }
            else
            {
                throw new Exception(entity.NAME + " modell doesn't exist!");
            }

            this.dB.SaveChanges();
        }
    }
}
