﻿// <copyright file="RepositoryModellLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository.Interfaces;

    /// <summary>
    /// Repostory of Modell table
    /// </summary>
    public class RepositoryModellLogic : IRepository<MODELL>
    {
        /// <summary>
        /// Database reference
        /// </summary>
        private readonly DBEntities dB;
        private int addnumberfromdelete;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryModellLogic"/> class.
        /// </summary>
        public RepositoryModellLogic()
        {
            this.dB = new DataBaseProviderFactory().DB;
            this.addnumberfromdelete = 0;
        }

        /// <summary>
        /// Gets List of Modell table
        /// </summary>
        public IEnumerable<MODELL> List => this.dB.MODELLs.ToList();

        /// <summary>
        /// Add a new instance of MODELL into the database
        /// </summary>
        /// <param name="be">be</param>
        /// <param name="cOMPANY_NAME">COMPANY_NAME</param>
        public void Add(MODELL be, string cOMPANY_NAME)
        {
            var check = from x in this.dB.COMPANies where x.NAME.Equals(cOMPANY_NAME) select x.ID;

            var distinct = from x in this.dB.MODELLs where x.NAME.Equals(be.NAME) select x;
            if (distinct.Count() > 0)
            {
                throw new Exception("There is already a modell under this name!");
            }

            if (check.Count() == 1)
            {
                decimal temp = this.dB.MODELLs.Count() == 0 ? 1 : this.dB.MODELLs.Count() + 1 + this.addnumberfromdelete;
                var cOMPANY_ID_temp = check.First();
                this.dB.MODELLs.Add(new MODELL
                {
                    ID = temp,
                    COMPANY_ID = cOMPANY_ID_temp,
                    NAME = be.NAME,
                    RELEASE = be.RELEASE,
                    ENGINE = be.ENGINE,
                    POWER = be.POWER,
                    PRICE = be.PRICE
                });
                this.dB.SaveChanges();
            }
            else if (check.Count() > 1)
            {
                throw new Exception("There are more than one of the " + cOMPANY_NAME);
            }
            else
            {
                throw new Exception(cOMPANY_NAME + " company doesn't exist!");
            }
        }

        /// <summary>
        /// Delete a row by NAME
        /// </summary>
        /// <param name="nAME">The NAME value of the deleting instance</param>
        public void Delete(string nAME)
        {
            var temp = from x in this.dB.MODELLs where x.NAME.Equals(nAME) select x;
            if (temp.Count() == 0)
            {
                throw new Exception("Item not found!");
            }

            var kek = temp.First();

            var temp2 = from x in this.dB.MODELL_EXTRA_CONNECT where x.MODELL_ID.Equals(kek.ID) select x;
            if (temp2.Count() == 0)
            {
                this.dB.MODELLs.Remove(temp.First());
                this.addnumberfromdelete++;
                this.dB.SaveChanges();
                throw new Exception("Item not found!");
            }

            foreach (var x in temp2)
            {
                this.dB.MODELL_EXTRA_CONNECT.Remove(x);
            }

            this.dB.MODELLs.Remove(temp.First());
            this.addnumberfromdelete++;
            this.dB.SaveChanges();
        }

        /// <summary>
        /// Update a row by the name of the modell
        /// </summary>
        /// <param name="entity">entity</param>
        public void Update(MODELL entity)
        {
            var check = from x in this.dB.MODELLs where x.NAME.Equals(entity.NAME) select x;
            if (check.Count() == 1)
            {
                var refer = this.dB.MODELLs.SingleOrDefault(b => b.NAME.Equals(entity.NAME));

                if (refer != null)
                {
                    refer.NAME = entity.NAME;
                    refer.RELEASE = entity.RELEASE;
                    refer.ENGINE = entity.ENGINE;
                    refer.POWER = entity.POWER;
                    refer.PRICE = entity.PRICE;
                    this.dB.SaveChanges();
                }
            }
            else if (check.Count() > 1)
            {
                throw new Exception("There are more than one of the " + entity.NAME);
            }
            else
            {
                throw new Exception(entity.NAME + " modell doesn't exist!");
            }
        }

        /// <summary>
        /// Find a roy by it's ID
        /// </summary>
        /// <param name="id">Name of the row</param>
        /// <returns>the row</returns>
        public MODELL FindById(int id)
        {
            var temp = from x in this.dB.MODELLs where x.ID == id select x;
            if (temp.Count() == 0 || temp == null)
            {
                return new MODELL { ID = -1 };
            }

            return temp.First();
        }

        /// <summary>
        /// Gets returns the connect table in a list format
        /// </summary>
        /// <returns>List of extras</returns>
        public List<MODELL_EXTRA_CONNECT> GetModellExtraJoin()
        {
            return this.dB.MODELL_EXTRA_CONNECT.ToList();
        }
    }
}
