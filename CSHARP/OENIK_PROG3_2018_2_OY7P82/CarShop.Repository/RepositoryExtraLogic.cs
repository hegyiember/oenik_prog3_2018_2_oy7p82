﻿// <copyright file="RepositoryExtraLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Repository.Interfaces;

    /// <summary>
    /// Repostory of Extra table
    /// </summary>
    public class RepositoryExtraLogic : IRepository<EXTRA>
    {
        private DBEntities dB;
        private int addnumberfromdelete;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryExtraLogic"/> class.
        /// </summary>
        public RepositoryExtraLogic()
        {
            this.dB = new DataBaseProviderFactory().DB;
            this.addnumberfromdelete = 0;
        }

        /// <summary>
        /// Gets sadasd
        /// </summary>
        public IEnumerable<EXTRA> List
        {
            get
            {
                return this.dB.EXTRAs.ToList();
            }
        }

        /// <summary>
        /// Returns the connect table of MODELL_EXTRA
        /// </summary>
        /// <returns>Connect table</returns>
        public List<MODELL_EXTRA_CONNECT> GetModellExtraJoin()
        {
            return this.dB.MODELL_EXTRA_CONNECT.ToList();
        }

        /// <summary>
        /// Find an element by ID
        /// </summary>
        /// <param name="id">element ID</param>
        /// <returns>element</returns>
        public EXTRA FindById(int id)
        {
            var temp = from x in this.dB.EXTRAs where x.ID == id select x;
            return temp.Count() == 0 || temp == null ? new EXTRA { ID = -1 } : temp.First();
        }

        /// <summary>
        /// Add a new element
        /// </summary>
        /// <param name="be">Element to be added</param>
        /// <param name="mODELL_NAME">NAme of the modell of the element</param>
        public void Add(EXTRA be, string mODELL_NAME)
        {
            var check = from x in this.dB.MODELLs where x.NAME.Equals(mODELL_NAME) select x.ID;

            var distinct = from x in this.dB.EXTRAs where x.NAME.Equals(be.NAME) select x;
            if (distinct.Count() > 0)
            {
                throw new Exception("Extra already exists!");
            }

            if (check.Count() == 1)
            {
                decimal temp = this.dB.EXTRAs.Count() == 0 ? 1 : this.dB.EXTRAs.Count() + 1 + addnumberfromdelete;
                this.dB.EXTRAs.Add(new EXTRA
                {
                    ID = temp,
                    NAME = be.NAME,
                    PRICE = be.PRICE,
                    COLOR = be.COLOR,
                    MULTIPLE_USE = be.MULTIPLE_USE
                });
                this.dB.SaveChanges();

                decimal temp2 = this.dB.MODELL_EXTRA_CONNECT.Count() == 0 ? 1 : this.dB.MODELL_EXTRA_CONNECT.Count() + 1 + addnumberfromdelete;
                var kek23 = check.First();
                this.dB.MODELL_EXTRA_CONNECT.Add(new MODELL_EXTRA_CONNECT
                {
                    ID = temp2,
                    MODELL_ID = kek23,
                    EXTRA_ID = temp
                });
                this.dB.SaveChanges();
            }
            else if (check.Count() > 1)
            {
                throw new Exception("There already is a modell under the name of " + mODELL_NAME);
            }
            else
            {
                throw new Exception(mODELL_NAME + " modell doesn't exist!");
            }
        }

        /// <summary>
        /// Delete a row by NAME
        /// </summary>
        /// <param name="nAME">The NAME value of the deleting instance</param>
        public void Delete(string nAME)
        {
            var temp = from x in this.dB.EXTRAs where x.NAME.Equals(nAME) select x;
            if (temp.Count() == 0)
            {
                throw new Exception("Item not found!");
            }

            var kek = temp.First();

            var temp2 = from x in this.dB.MODELL_EXTRA_CONNECT where x.EXTRA_ID.Equals(kek.ID) select x;
            if (temp.Count() == 0)
            {
                this.dB.EXTRAs.Remove(kek);
                this.addnumberfromdelete++;
                this.dB.SaveChanges();
                throw new Exception("This extra doesn't belong to any car!");
            }

            foreach (var x in temp2)
            {
                this.dB.MODELL_EXTRA_CONNECT.Remove(x);
            }

            this.dB.EXTRAs.Remove(kek);
            this.addnumberfromdelete++;
            this.dB.SaveChanges();
        }

        /// <summary>
        /// Update a row by name of the element
        /// </summary>
        /// <param name="entity">Element to be updated</param>
        public void Update(EXTRA entity)
        {
            var check = from x in this.dB.EXTRAs where x.NAME.Equals(entity.NAME) select x;
            if (check.Count() == 1)
            {
                var refer = this.dB.EXTRAs.SingleOrDefault(b => b.NAME.Equals(entity.NAME));
                if (refer != null)
                {
                    refer.NAME = entity.NAME;
                    refer.PRICE = entity.PRICE;
                    refer.COLOR = entity.COLOR;
                    refer.MULTIPLE_USE = entity.MULTIPLE_USE;
                    this.dB.SaveChanges();
                }
            }
            else if (check.Count() > 1)
            {
                throw new Exception("There are more than one of the " + entity.NAME);
            }
            else
            {
                throw new Exception(entity.NAME + " extra doesn't exist!");
            }
        }
    }
}