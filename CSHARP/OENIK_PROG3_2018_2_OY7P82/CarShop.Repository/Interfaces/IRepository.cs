﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Repostory interface, CRUD műveletek: Create,Read,Update,Delete
    /// </summary>
    /// <typeparam name="T">Table types</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets list
        /// Gets </summary>
        IEnumerable<T> List { get; }

        /// <summary>
        /// Gets ModellExtraJoin
        /// </summary>
        /// <returns>List of connection table</returns>
        List<MODELL_EXTRA_CONNECT> GetModellExtraJoin();

        /// <summary>
        /// CRUB
        /// </summary>
        /// <param name="entity">entity</param>
        /// <param name="nAME">nAME</param>
        void Add(T entity, string nAME);

        /// <summary>
        /// CRUB
        /// </summary>
        /// <param name="nAME">nAME</param>
        void Delete(string nAME);

        /// <summary>
        /// CRUB
        /// </summary>
        /// <param name="entity">entity</param>
        void Update(T entity);

        /// <summary>
        /// CRUB
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>element</returns>
        T FindById(int id);
    }
}
