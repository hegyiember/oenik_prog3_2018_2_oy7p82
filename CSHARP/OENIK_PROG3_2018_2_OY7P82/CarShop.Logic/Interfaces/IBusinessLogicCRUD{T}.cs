﻿// <copyright file="IBusinessLogicCRUD{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /*
    Példa funkciólista:
    - Márkák listázása / hozzáadása / módosítása / törlése
    - Modellek listázása / hozzáadása / módosítása / törlése
    - Extrák listázása / hozzáadása / módosítása / törlése
    - Modell-extra összerendelések listázása / hozzáadása / törlése
    - Legyen lehetőségünk kiírni minden autóhoz az autó TELJES árát is: alapár + a rajta lévő extrák összára
    - Legyen lehetőségünk kiírni márkánként az autók átlagos alapárát
    - Legyen lehetőségünk kiírni az extrák kategórianeveként csoportosítva azt, hogy melyik kategória hányszor van autókhoz kapcsolva
    */

    // Mint a kórházakban, nyom az ember egy entert és lejjeb ugrik az adat a kiírta elé, így nem kell generikusan csinálnod mert helyben hozod klétre a tábla objektumot

    /// <summary>
    /// Interface of the CRUD method Budiness Logic
    /// </summary>
    /// <typeparam name="T">Table</typeparam>
    public interface IBusinessLogicCRUD<T>
    {
        /// <summary>
        /// List the T elements of a table
        /// </summary>
        /// <returns>List of T elements</returns>
        IEnumerable<T> List();

        /// <summary>
        /// Add a new row
        /// </summary>
        /// <param name="entity">T type row entity</param>
        /// <param name="nAME">Name of the T</param>
        void Add(T entity, string nAME);

        /// <summary>
        /// Update a T element in a row
        /// </summary>
        /// <param name="entity">Name of the T element in the row</param>
        void Update(T entity);

        /// <summary>
        /// Delete a T element
        /// </summary>
        /// <param name="nAME">The name in the T element</param>
        void Delete(string nAME);

        /// <summary>
        /// Find a T by ID
        /// </summary>
        /// <param name="id">ID of the T</param>
        /// <returns>T  with the matching ID</returns>
        T FindById(int id);
    }
}
