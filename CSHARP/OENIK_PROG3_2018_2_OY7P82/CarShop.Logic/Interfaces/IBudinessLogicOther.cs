﻿// <copyright file="IBudinessLogicOther.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Business logic interface
    /// </summary>
    public interface IBudinessLogicOther
    {
        /// <summary>
        /// Returns the full prices of car modells
        /// </summary>
        /// <returns>List of full prices</returns>
        Dictionary<string, int> ModellFullprice();

        /// <summary>
        /// Return of average prices of companies
        /// </summary>
        /// <returns>List of average prices of companies</returns>
        Dictionary<string, int> CompanyAveragePrice();

        /// <summary>
        /// return the number of colours by modells
        /// </summary>
        /// <returns>List of colour numbers</returns>
        Dictionary<string, int> ExtraxDifferentColoursByModells();
    }
}
