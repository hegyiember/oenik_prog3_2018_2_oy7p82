﻿// <copyright file="BusinessNonCRUDLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic.Interfaces;
    using CarShop.Repository;
    using CarShop.Repository.Interfaces;

    /// <summary>
    /// Business logic for the non-CRUD methods
    /// </summary>
    public class BusinessNonCRUDLogic : IBudinessLogicOther
    {
        private IRepository<MODELL> rML;
        private IRepository<EXTRA> rEL;
        private IRepository<COMPANY> rCL;
        private List<MODELL_EXTRA_CONNECT> joined;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessNonCRUDLogic"/> class.
        /// </summary>
        public BusinessNonCRUDLogic()
        {
            this.rML = new RepositoryModellLogic();
            this.rEL = new RepositoryExtraLogic();
            this.rCL = new RepositoryCompanyLogic();
            this.joined = this.rML.GetModellExtraJoin();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessNonCRUDLogic"/> class.
        /// </summary>
        /// <param name="rML">Repostory logic for the Modell table</param>
        /// <param name="rEL">Repostory logic for the Extra table</param>
        /// <param name="rCL">Repostory logic for the Company table</param>
        /// <param name="joined">Repostory logic for the Connecting table</param>
        public BusinessNonCRUDLogic(IRepository<MODELL> rML, IRepository<EXTRA> rEL, IRepository<COMPANY> rCL, List<MODELL_EXTRA_CONNECT> joined)
        {
            this.rML = rML;
            this.rEL = rEL;
            this.rCL = rCL;
            this.joined = joined;
        }

        /// <summary>
        /// Average car price by companies
        /// </summary>
        /// <returns>Average car price</returns>
        public Dictionary<string, int> CompanyAveragePrice()
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();
            var kek = from q in
                          from x in this.rCL.List join y in this.rML.List on x.ID equals y.COMPANY_ID select new { x.NAME, y.PRICE }
                      group q by q.NAME into g
                      select new
                      {
                          Name = g.Key,
                          Average = g.Average(z => z.PRICE)
                      };
            foreach (var x in kek)
            {
                dic.Add(x.Name, Convert.ToInt32(x.Average));
            }

            return dic;
        }

        /// <summary>
        /// How many different colored extras does contain a car
        /// </summary>
        /// <returns>number of car instalations by extra groups</returns>
        public Dictionary<string, int> ExtraxDifferentColoursByModells()
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();

            // Hopefilly distinct makes the job and deletes the same ID and CXOlor elements
            var kek = (from x in this.joined join y in this.rEL.List on x.EXTRA_ID equals y.ID select new { x.MODELL_ID, y.COLOR }).ToList().Distinct();
            int difcol;
            foreach (var x in this.rML.List)
            {
                difcol = 0;
                foreach (var y in kek)
                {
                    if (y.MODELL_ID == x.ID)
                    {
                        difcol++;
                    }
                }

                dic.Add(x.NAME, difcol);
            }

            return dic;
        }

        /// <summary>
        /// Full price of a car (basic + extras)
        /// </summary>
        /// <returns>Full price</returns>
        public Dictionary<string, int> ModellFullprice()
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();
            foreach (var x in this.rML.List)
            {
                foreach (var y in this.rEL.List)
                {
                    if (y.ID == x.ID)
                    {
                        dic.Add(x.NAME, Convert.ToInt32(x.PRICE + this.rEL.FindById(Convert.ToInt32(x.ID)).PRICE));
                        break;
                    }
                }
            }

            return dic;
        }
    }
}
