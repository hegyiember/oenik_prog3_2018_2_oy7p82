﻿// <copyright file="BusinessModellLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic.Interfaces;
    using CarShop.Repository;
    using CarShop.Repository.Interfaces;

    /// <summary>
    /// Business logic for the Modell table CRUD methods
    /// </summary>
    public class BusinessModellLogic : IBusinessLogicCRUD<MODELL>
    {
        private IRepository<MODELL> rML;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessModellLogic"/> class.
        /// </summary>
        public BusinessModellLogic()
        {
            this.rML = new RepositoryModellLogic();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessModellLogic"/> class.
        /// </summary>
        /// <param name="be">Repostory logic for the Modell table</param>
        public BusinessModellLogic(IRepository<MODELL> be)
        {
            this.rML = be;
        }

        /// <summary>
        /// Add a new row
        /// </summary>
        /// <param name="entity">Modell type row</param>
        /// <param name="nAME">Name of the company it belongs</param>
        public void Add(MODELL entity, string nAME)
        {
            this.rML.Add(entity, nAME);
        }

        /// <summary>
        /// Delete a row
        /// </summary>
        /// <param name="nAME">name of the element</param>
        public void Delete(string nAME)
        {
            this.rML.Delete(nAME);
        }

        /// <summary>
        /// Find a row by ID
        /// </summary>
        /// <param name="id">ID of the element</param>
        /// <returns>The element with the matching ID</returns>
        public MODELL FindById(int id)
        {
            return this.rML.FindById(id);
        }

        /// <summary>
        /// Return of a list of table elements
        /// </summary>
        /// <returns>List of table elements</returns>
        public IEnumerable<MODELL> List()
        {
            return this.rML.List;
        }

        /// <summary>
        /// Update an element in a row
        /// </summary>
        /// <param name="entity">Name of the element in the row</param>
        public void Update(MODELL entity)
        {
            this.rML.Update(entity);
        }
    }
}
