﻿// <copyright file="BusinessExtraLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic.Interfaces;
    using CarShop.Repository;
    using CarShop.Repository.Interfaces;

    /// <summary>
    /// Business logic for the Extra table CRUD methods
    /// </summary>
    public class BusinessExtraLogic : IBusinessLogicCRUD<EXTRA>
    {
        private IRepository<EXTRA> rML;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessExtraLogic"/> class.
        /// </summary>
        public BusinessExtraLogic()
        {
            this.rML = new RepositoryExtraLogic();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessExtraLogic"/> class.
        /// </summary>
        /// <param name="be">Repostory logic for the Extra table</param>
        public BusinessExtraLogic(IRepository<EXTRA> be)
        {
            this.rML = be;
        }

        /// <summary>
        /// Add a new row
        /// </summary>
        /// <param name="entity">Extra type row entity</param>
        /// <param name="nAME">Name of the modell the extra belongs</param>
        public void Add(EXTRA entity, string nAME)
        {
            this.rML.Add(entity, nAME);
        }

        /// <summary>
        /// Delete a row
        /// </summary>
        /// <param name="nAME">name of the element</param>
        public void Delete(string nAME)
        {
            this.rML.Delete(nAME);
        }

        /// <summary>
        /// Find a row by ID
        /// </summary>
        /// <param name="id">ID of the element</param>
        /// <returns>The element with the matching ID</returns>
        public EXTRA FindById(int id)
        {
            return this.rML.FindById(id);
        }

        /// <summary>
        /// Return of a list of table elements
        /// </summary>
        /// <returns>List of table elements</returns>
        public IEnumerable<EXTRA> List()
        {
            return this.rML.List;
        }

        /// <summary>
        /// Update an element in a row
        /// </summary>
        /// <param name="entity">Name of the element in the row</param>
        public void Update(EXTRA entity)
        {
            this.rML.Update(entity);
        }

        /// <summary>
        /// Return the conenct table as a list
        /// </summary>
        /// <returns>Connect table as a list</returns>
        public IEnumerable<MODELL_EXTRA_CONNECT> ConnectList()
        {
            return this.rML.GetModellExtraJoin();
        }
    }
}
