﻿// <copyright file="BusinessCompanyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic.Interfaces;
    using CarShop.Repository;
    using CarShop.Repository.Interfaces;

    /// <summary>
    /// Business logic for the Company table CRUD methods
    /// </summary>
    public class BusinessCompanyLogic : IBusinessLogicCRUD<COMPANY>
    {
        private IRepository<COMPANY> rML;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessCompanyLogic"/> class.
        /// </summary>
        public BusinessCompanyLogic()
        {
            this.rML = new RepositoryCompanyLogic();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessCompanyLogic"/> class.
        /// </summary>
        /// <param name="be">Repostory logic for the Company table</param>
        public BusinessCompanyLogic(IRepository<COMPANY> be)
        {
            this.rML = be;
        }

        /// <summary>
        /// Add a new row
        /// </summary>
        /// <param name="entity">Company type row entity</param>
        /// <param name="nAME">Name of the company</param>
        public void Add(COMPANY entity, string nAME)
        {
            this.rML.Add(entity, nAME);
        }

        /// <summary>
        /// Delete a row
        /// </summary>
        /// <param name="nAME">name of the element</param>
        public void Delete(string nAME)
        {
            this.rML.Delete(nAME);
        }

        /// <summary>
        /// Find a row by ID
        /// </summary>
        /// <param name="id">ID of the element</param>
        /// <returns>The element with the matching ID</returns>
        public COMPANY FindById(int id)
        {
            return this.rML.FindById(id);
        }

        /// <summary>
        /// Return of a list of table elements
        /// </summary>
        /// <returns>List of table elements</returns>
        public IEnumerable<COMPANY> List()
        {
            return this.rML.List;
        }

        /// <summary>
        /// Update an element in a row
        /// </summary>
        /// <param name="entity">Name of the element in the row</param>
        public void Update(COMPANY entity)
        {
            this.rML.Update(entity);
        }
    }
}
