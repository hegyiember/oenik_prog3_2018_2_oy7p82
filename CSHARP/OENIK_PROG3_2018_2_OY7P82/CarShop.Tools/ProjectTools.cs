﻿// <copyright file="ProjectTools.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    ///  Emums fr the Program class to function in the MainMenu
    /// </summary>
    public class ProjectTools
    {
        /// <summary>
        /// The main enums, representing the tables
        /// </summary>
        public enum TableTypes
        {
            /// <summary>
            /// Modell table
            /// </summary>
            MODELL,

            /// <summary>
            /// Extra table
            /// </summary>
            EXTRA,

            /// <summary>
            /// Company table
            /// </summary>
            COMPANY,

            /// <summary>
            /// Default value
            /// </summary>
            PLACEHOLDER
        }

        /// <summary>
        /// Secondary enums, representig the non-CRUD method for the MainMenu
        /// </summary>
        public enum NonCRUDTypes
        {
            /// <summary>
            /// ModellFullprice algoríthm
            /// </summary>
            ModellFullprice,

            /// <summary>
            /// CompanyAveragePrice algoríthm
            /// </summary>
            CompanyAveragePrice,

            /// <summary>
            /// ExtraxDifferentColoursByModells algoríthm
            /// </summary>
            ExtraxDifferentColoursByModells,

            /// <summary>
            /// Default value
            /// </summary>
            PLACEHOLDER
        }
    }
}
