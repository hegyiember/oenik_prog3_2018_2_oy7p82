﻿// <copyright file="DataBaseProviderFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Tools;

    /// <summary>
    /// Database provider
    /// </summary>
    public class DataBaseProviderFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataBaseProviderFactory"/> class.
        /// </summary>
        public DataBaseProviderFactory() => this.DB = new DBEntities();

        /// <summary>
        /// Gets or sets database
        /// </summary>
        public DBEntities DB { get; set; }
    }
}