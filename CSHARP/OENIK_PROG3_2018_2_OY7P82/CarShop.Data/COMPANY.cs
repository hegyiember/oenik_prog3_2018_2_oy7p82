namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class COMPANY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public COMPANY()
        {
            this.MODELLs = new HashSet<MODELL>();
        }
    
        public decimal ID { get; set; }
        public string NAME { get; set; }
        public string COUNTRY { get; set; }
        public string URL { get; set; }
        public Nullable<decimal> FOUNDATION { get; set; }
        public Nullable<decimal> YEARLY_INCOME { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MODELL> MODELLs { get; set; }
    }
}
