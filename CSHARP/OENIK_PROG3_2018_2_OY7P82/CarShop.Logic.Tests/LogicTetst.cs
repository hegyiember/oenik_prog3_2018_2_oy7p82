﻿// <copyright file="LogicTetst.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;
    using CarShop.Logic.Interfaces;
    using CarShop.Repository;
    using CarShop.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class fopr the repostoy
    /// </summary>
    [TestFixture]
    public class LogicTetst
    {
        private RepositoryModellLogic rML;
        private RepositoryCompanyLogic rCL;
        private RepositoryExtraLogic rEL;
        private Mock<IRepository<COMPANY>> mockingCOMPANY;
        private Mock<IRepository<MODELL>> mockingMODELL;
        private Mock<IRepository<EXTRA>> mockingEXTRA;
        private BusinessCompanyLogic logicMockingCOMPANY;
        private BusinessModellLogic logicMockingMODELL;
        private BusinessExtraLogic logicMockingEXTRA;
        private BusinessNonCRUDLogic logicMockingNONCRUD;

        /// <summary>
        /// Gets MOQ for CRUD logic
        /// </summary>
        public BusinessNonCRUDLogic LogicNONCRUD { get; }

        /// <summary>
        /// Gets MOQ for non-CRUD logic
        /// </summary>
        public Mock<IBudinessLogicOther> MockingNONCRUD { get; }

        /// <summary>
        /// Man setup of MOQ testing
        /// </summary>
        [OneTimeSetUp]
        public void Setup()
        {
            this.rML = new RepositoryModellLogic();
            this.rCL = new RepositoryCompanyLogic();
            this.rEL = new RepositoryExtraLogic();

            List<COMPANY> iQueryable_LIST_RCL = new List<COMPANY>()
            {
                new COMPANY() { ID = 1, NAME = "AS", COUNTRY = "HUN", FOUNDATION = 1955, YEARLY_INCOME = 50000 },
                new COMPANY() { ID = 2, NAME = "BS", COUNTRY = "SWE", FOUNDATION = 1966, YEARLY_INCOME = 60000 },
                new COMPANY() { ID = 3, NAME = "CS", COUNTRY = "IRE", FOUNDATION = 1977, YEARLY_INCOME = 70000 },
                new COMPANY() { ID = 4, NAME = "DS", COUNTRY = "GBR", FOUNDATION = 1988, YEARLY_INCOME = 80000 },
            };

            List<MODELL> iQueryable_LIST_RML = new List<MODELL>()
            {
                new MODELL { ID = 1,  NAME = "A", COMPANY_ID = 1, PRICE = 1000, POWER = 3245, ENGINE = 6500, RELEASE = 1988 },
                new MODELL { ID = 2,  NAME = "B", COMPANY_ID = 2,  PRICE = 1500, POWER = 3352, ENGINE = 6755, RELEASE = 1989 },
                new MODELL { ID = 3,  NAME = "C", COMPANY_ID = 2,  PRICE = 2000, POWER = 3459, ENGINE = 7010, RELEASE = 1990 },
                new MODELL { ID = 4,  NAME = "D", COMPANY_ID = 1,  PRICE = 2500, POWER = 3566, ENGINE = 7265, RELEASE = 1991 },
                new MODELL { ID = 5,  NAME = "E", COMPANY_ID = 3,  PRICE = 3000, POWER = 3673, ENGINE = 7520, RELEASE = 1992 },
                new MODELL { ID = 6,  NAME = "F", COMPANY_ID = 4,  PRICE = 3500, POWER = 3780, ENGINE = 7775, RELEASE = 1993 },
                new MODELL { ID = 7,  NAME = "G", COMPANY_ID = 3,  PRICE = 4000, POWER = 3887, ENGINE = 8030, RELEASE = 1994 },
                new MODELL { ID = 8,  NAME = "H", COMPANY_ID = 2,  PRICE = 4500, POWER = 3994, ENGINE = 8285, RELEASE = 1995 },
                new MODELL { ID = 9,  NAME = "I", COMPANY_ID = 4,  PRICE = 5000, POWER = 4101, ENGINE = 8540, RELEASE = 1996 },
                new MODELL { ID = 10,  NAME = "J", COMPANY_ID = 1,  PRICE = 5500, POWER = 4208, ENGINE = 8795, RELEASE = 1997 },
            };

            List<EXTRA> iQueryable_LIST_REL = new List<EXTRA>()
            {
                new EXTRA() { ID = 1,  NAME = "ETH", COLOR = "D", PRICE = 555, MULTIPLE_USE = true },
                new EXTRA() { ID = 2,  NAME = "AT", COLOR = "A", PRICE = 1000, MULTIPLE_USE = true },
                new EXTRA() { ID = 3,  NAME = "BT", COLOR = "A", PRICE = 200, MULTIPLE_USE = false },
                new EXTRA() { ID = 4,  NAME = "CT", COLOR = "C", PRICE = 400, MULTIPLE_USE = true },
                new EXTRA() { ID = 5,  NAME = "DT", COLOR = "S", PRICE = 200, MULTIPLE_USE = false },
                new EXTRA() { ID = 6,  NAME = "ET", COLOR = "D", PRICE = 555, MULTIPLE_USE = true },
                new EXTRA() { ID = 7,  NAME = "FT", COLOR = "S", PRICE = 222, MULTIPLE_USE = true },
                new EXTRA() { ID = 8,  NAME = "FTH", COLOR = "C", PRICE = 400, MULTIPLE_USE = true },
                new EXTRA() { ID = 9,  NAME = "DTH", COLOR = "S", PRICE = 200, MULTIPLE_USE = false }
            };

            List<MODELL_EXTRA_CONNECT> iQueryable_LIST_JOINED = new List<MODELL_EXTRA_CONNECT>()
            {
                new MODELL_EXTRA_CONNECT() { ID = 1, EXTRA_ID = 1, MODELL_ID = 1 },
                new MODELL_EXTRA_CONNECT() { ID = 2, EXTRA_ID = 2, MODELL_ID = 2 },
                new MODELL_EXTRA_CONNECT() { ID = 3, EXTRA_ID = 3, MODELL_ID = 3 },
                new MODELL_EXTRA_CONNECT() { ID = 4, EXTRA_ID = 4, MODELL_ID = 4 },
                new MODELL_EXTRA_CONNECT() { ID = 5, EXTRA_ID = 5, MODELL_ID = 5 },
                new MODELL_EXTRA_CONNECT() { ID = 6, EXTRA_ID = 6, MODELL_ID = 6 },
                new MODELL_EXTRA_CONNECT() { ID = 7, EXTRA_ID = 7, MODELL_ID = 7 },
                new MODELL_EXTRA_CONNECT() { ID = 8, EXTRA_ID = 8, MODELL_ID = 8 },
                new MODELL_EXTRA_CONNECT() { ID = 9, EXTRA_ID = 9, MODELL_ID = 9 },
            };

            this.mockingCOMPANY = new Mock<IRepository<COMPANY>>();
            this.mockingMODELL = new Mock<IRepository<MODELL>>();
            this.mockingEXTRA = new Mock<IRepository<EXTRA>>();

            // As the mocking doesn't use the real repository, we need to tel it what would be the return value. This is the setup process.
            this.mockingCOMPANY.Setup(x => x.List).Returns(iQueryable_LIST_RCL.AsQueryable<COMPANY>());
            this.mockingMODELL.Setup(x => x.List).Returns(iQueryable_LIST_RML.AsQueryable<MODELL>());
            this.mockingEXTRA.Setup(x => x.List).Returns(iQueryable_LIST_REL.AsQueryable<EXTRA>());

            this.logicMockingCOMPANY = new BusinessCompanyLogic(this.mockingCOMPANY.Object);
            this.logicMockingMODELL = new BusinessModellLogic(this.mockingMODELL.Object);
            this.logicMockingEXTRA = new BusinessExtraLogic(this.mockingEXTRA.Object);

            this.logicMockingNONCRUD = new BusinessNonCRUDLogic(this.mockingMODELL.Object, this.mockingEXTRA.Object, this.mockingCOMPANY.Object, iQueryable_LIST_JOINED);
        }

        // Test of repostory CRUD with MOQ: ADD

        /// <summary>
        /// Testing whether the ADD function for COMPANY is working
        /// </summary>
        [Test]
        public void WhenInsertCOMPANYInBusinessLogicIsCalled_TheRepoVersionIsCalledToo()
        {
            this.logicMockingCOMPANY.Add(new COMPANY { NAME = "TEST0", COUNTRY = "HUN", FOUNDATION = 1935, YEARLY_INCOME = 4000 }, "TEST0");
            this.mockingCOMPANY.Verify(x => x.Add(It.IsAny<COMPANY>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Testing whether the ADD function for COMPANY throws no exception
        /// </summary>
        [Test]
        public void WhenInsertCOMPANYInBusinessLogicIsCalled_TheRepoVersionIsCalledToo_ExceptionNo()
        {
            Assert.That(() => this.logicMockingCOMPANY.Add(new COMPANY { NAME = "ASAAAAA", COUNTRY = "HUN", FOUNDATION = 1935, YEARLY_INCOME = 4000 }, "Toyota"), Throws.Nothing);
        }

        /// <summary>
        /// Testing whether the ADD function for MODELL is working
        /// </summary>
        [Test]
        public void WhenInsertMODELLInBusinessLogicIsCalled_TheRepoVersionIsCalledToo()
        {
            this.logicMockingMODELL.Add(new MODELL { NAME = "TEST0", PRICE = 1020, POWER = 3245, ENGINE = 6500, RELEASE = 1988 }, "Toyota");
            this.mockingMODELL.Verify(x => x.Add(It.IsAny<MODELL>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Testing whether the ADD function for MODELL throws no exception
        /// </summary>
        [Test]
        public void WhenInsertMODELLInBusinessLogicIsCalled_TheRepoVersionIsCalledToo_ExceptionNo()
        {
            Assert.That(() => this.logicMockingMODELL.Add(new MODELL { NAME = "ABAAA", PRICE = 1020, POWER = 3245, ENGINE = 6500, RELEASE = 1988 }, "Toyota"), Throws.Nothing);
        }

        /// <summary>
        /// Testing whether the ADD function for EXTRA is working
        /// </summary>
        [Test]
        public void WhenInsertEXTRAInBusinessLogicIsCalled_TheRepoVersionIsCalledToo()
        {
            this.logicMockingEXTRA.Add(new EXTRA { NAME = "TEST0", COLOR = "A", PRICE = 1000, MULTIPLE_USE = true }, "Toyota");
            this.mockingEXTRA.Verify(x => x.Add(It.IsAny<EXTRA>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Testing whether the ADD function for EXTRA throws no exception
        /// </summary>
        [Test]
        public void WhenInsertEXTRAInBusinessLogicIsCalled_TheRepoVersionIsCalledToo_ExceptionNo()
        {
            Assert.That(() => this.logicMockingEXTRA.Add(new EXTRA { NAME = "ATAAAA", COLOR = "A", PRICE = 1000, MULTIPLE_USE = true }, "Toyota"), Throws.Nothing);
        }

        // Test of repostory CRUD with MOQ: UPDATE

        /// <summary>
        /// Testing whether the UPDATE function for COMPANY is working
        /// </summary>
        [Test]
        public void WhenUpdateCOMPANYInBusinessLogicIsCalled_TheRepoVersionIsCalledToo()
        {
            this.logicMockingCOMPANY.Update(new COMPANY { NAME = "TEST_U1", COUNTRY = "HUN_U", FOUNDATION = 1995, YEARLY_INCOME = 4030 });
            this.mockingCOMPANY.Verify(x => x.Update(It.IsAny<COMPANY>()), Times.Once);
        }

        /// <summary>
        /// Testing whether the UPDATE function for COMPANY throws no exception
        /// </summary>
        [Test]
        public void WhenUpdateMODELLInBusinessLogicIsCalled_TheRepoVersionIsCalledToo()
        {
            this.logicMockingMODELL.Update(new MODELL { NAME = "TEST1_U", PRICE = 1320, POWER = 3245, ENGINE = 6600, RELEASE = 1988 });
            this.mockingMODELL.Verify(x => x.Update(It.IsAny<MODELL>()), Times.Once);
        }

        /// <summary>
        /// Testing whether the UPDATE function for EXTRA is working
        /// </summary>
        [Test]
        public void WhenUpdateEXTRAInBusinessLogicIsCalled_TheRepoVersionIsCalledToo()
        {
            this.logicMockingEXTRA.Update(new EXTRA { NAME = "TEST1_U", COLOR = "A_U", PRICE = 1040, MULTIPLE_USE = false });
            this.mockingEXTRA.Verify(x => x.Update(It.IsAny<EXTRA>()), Times.Once);
        }

        /// <summary>
        /// Testing CompanyAveragePrice method, wherteh it gives back the given first element in the list
        /// </summary>
        [Test]
        public void CompanyAveragePriceTest1()
        {
            Assert.That(this.logicMockingNONCRUD.CompanyAveragePrice().First().Key, Is.EqualTo("AS"));
        }

        /// <summary>
        /// Testing CompanyAveragePrice method, wherteh it gives back the wrong first element in the list
        /// </summary>
        [Test]
        public void CompanyAveragePriceTest2()
        {
            Assert.That(this.logicMockingNONCRUD.CompanyAveragePrice().First().Key, Is.Not.EqualTo("ASsdsd"));
        }

        /// <summary>
        /// Testing ExtraxDifferentColoursByModells method, wherteh it gives back the enough number of elements
        /// </summary>
        [Test]
        public void ExtraxDifferentColoursByModellsTest1()
        {
            Assert.That(this.logicMockingNONCRUD.ExtraxDifferentColoursByModells().Count(), Is.GreaterThan(1));
        }

        /// <summary>
        /// Testing ExtraxDifferentColoursByModells method, wherteh it gives back the possible number of elements
        /// </summary>
        [Test]
        public void ExtraxDifferentColoursByModellsTest2()
        {
            Assert.That(this.logicMockingNONCRUD.ExtraxDifferentColoursByModells().First().Value, Is.InRange(1, 10));
        }

        /// <summary>
        /// Testing ModellFullprice method, whether it throws an exception during calculating or not
        /// </summary>
        [Test]
        public void ModellFullpricetest1()
        {
            // Assert.That(logic_mocking_NONCRUD.ModellFullprice().Count(), Is.EqualTo(10));
            Assert.That(() => this.logicMockingNONCRUD.ModellFullprice(), Throws.TypeOf<NullReferenceException>());
        }
    }
}
